module Data.Aeson.TH.Modifiers (hs2c, repl, completeTypField) where

import Data.Char
import Data.Strings

completeTypField :: String -> String
completeTypField fld | fld == "typ" = "type"
                     | otherwise = fld

repl :: [(String, String)] -> String -> String
repl replacements str = foldr repl' str replacements
    where repl' :: (String, String) -> String -> String
          repl' (needle, replacement) = strReplace needle replacement

hs2c :: String -> String
hs2c str = hs2c1 str

hs2c1 :: String -> String
hs2c1 (c:str) = (toLower c):(hs2cR str)
hs2c1 [] = ""

hs2cR :: String -> String
hs2cR (c:str) 
    | isUpper c || isDigit c = "_"++[toLower c]++(hs2cR str)
    | otherwise = c:(hs2cR str)
hs2cR [] = ""
